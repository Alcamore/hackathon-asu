/** Map Variables */
var
    WALL  = 0, // Move into wall and map restarts
    PATH  = 1, // Follow path to win
    END   = 2, // User beats level at this point

MAPS = [
    /* For now, max size is 6 */
    /* Map 0 */
    [
        [1, 0, 0],
        [1, 1, 1],
        [0, 0, 2]
    ],

    /* Map 1 */
    [
        [1, 0, 0, 0],
        [1, 0, 0, 0],
        [1, 1, 1, 0],
        [0, 0, 1, 2]
    ],

    /* Map 2 */
    [
        [1, 0, 0, 0, 0],
        [1, 0, 1, 1, 1],
        [1, 0, 1, 0, 1],
        [1, 1, 1, 0, 1],
        [0, 0, 0, 0, 2]
    ],

    /* Map 3 */
    [
        [1, 0, 0, 0, 0, 0],
        [1, 1, 0, 1, 1, 1],
        [0, 1, 0, 1, 0, 1],
        [0, 1, 0, 1, 0, 1],
        [0, 1, 1, 1, 0, 1],
        [0, 0, 0, 0, 0, 2]
    ]
];

var currentMap = 0;
var playerLocation = [0,0];

function movePlayer(key) {
    var size = currentMap+3;
    var previousLocation = playerLocation;
    var nextMove;
    switch(key) {
        case "ArrowRight":
            if (playerLocation[1] !== size-1) {
                nextMove = MAPS[currentMap][playerLocation[0]][playerLocation[1]+1];
                if (nextMove !== 0)
                {
                    playerLocation[1]++;
                    break;
                }
            }
            restartGame();
            break;
        case "ArrowLeft":
            if (playerLocation[1] !== 0) {
                nextMove = MAPS[currentMap][playerLocation[0]][playerLocation[1]-1];
                if (nextMove !== 0)
                {
                    playerLocation[1]--;
                    break;
                }
            }
            restartGame();
            break;
        case "ArrowUp":
            if (playerLocation[0] !== 0) {
                nextMove = MAPS[currentMap][playerLocation[0]-1][playerLocation[1]];
                if (nextMove !== 0)
                {
                    playerLocation[0]--;
                    break;
                }
            }
            restartGame();
            break;
        case "ArrowDown":
            if (playerLocation[0] !== size-1) {
                nextMove = MAPS[currentMap][playerLocation[0]+1][playerLocation[1]];
                if (nextMove !== 0)
                {
                    playerLocation[0]++;
                    break;
                }
            }
            restartGame();
            break;
        default:
            return playerLocation;
    }

    // Check for victory, reset map when we run out
    if (MAPS[currentMap][playerLocation[0]][playerLocation[1]] === END) {
        console.log("Map " + currentMap + " victory!");
        playerLocation = [0,0];
        if (currentMap === MAPS.length-1) {
            currentMap = 0;
        } else {
            currentMap++;
        }
        drawmap(document.getElementById('map'));
    }
    return previousLocation;
}

function restartGame() {
    playerLocation = [0,0];
    drawmap(document.getElementById('map'));
}
