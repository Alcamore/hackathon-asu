var size = 800; // Size of canvas (length or width)

var wallColor = "#222222";
var pathColor = "#FFFFFF";
var exitColor = "#00FF00";
var playerColor = "#FFFF00";
var playerPrevious = "#0000FF";

var canvas = document.getElementById('map');
var context = canvas.getContext('2d');

document.onkeydown = function(event) {
    // Capture the event, and ensure we have an event
    event = event || window.event;
    // Capture value of the key
    var key = event.key || event.which || event.keyCode;

    console.log("Key pressed: " + key);

    if (key == "ArrowLeft") {
        drawPlayer(movePlayer(key));
    }

    if (key == "ArrowUp") {
        drawPlayer(movePlayer(key));
    }

    if (key == "ArrowRight") {
        drawPlayer(movePlayer(key));
    }

    if (key == "ArrowDown") {
        drawPlayer(movePlayer(key));
    }
};

function drawmap(canvas)
{
    context.fillStyle = wallColor;
    context.fillRect(0,0,size,size);
    var mapSize = currentMap+3;
    var i, j;

    var delay = 1000-100*mapSize; // In milliseconds
    if (delay < 100)
        delay = 100;

    // Draw grid
    for (i = 0; i < mapSize; i++) {
        for (j = 0; j < mapSize; j++) {
            if (MAPS[currentMap][i][j] == PATH) {
                context.fillStyle = pathColor;
                context.fillRect(size*(j/mapSize), size*(i/mapSize),
                    size/mapSize, size/mapSize);
            }
        }
    }

    context.fillStyle = exitColor;
    context.fillRect(size - size/mapSize, size - size/mapSize,
            size/mapSize, size/mapSize);

    drawPlayer(playerLocation);
    setTimeout(function() {
        // Cover the path after a short delay
        context.fillStyle = wallColor;
        context.fillRect(0, 0, size, size);

        // Draw the exit
        context.fillStyle = exitColor;

        context.fillRect(size - size/mapSize, size - size/mapSize,
            size/mapSize, size/mapSize);

        drawPlayer(playerLocation);
    }, delay);
}

//Update the player on screen
function drawPlayer(previousLocation) {
    var mapSize = currentMap+3;
    //var oldPosition = [size * (previousLocation[0]/mapSize),
    //                   size * (previousLocation[1]/mapSize)];
    var newPosition = [size * (playerLocation[0]/mapSize),
                       size * (playerLocation[1]/mapSize)];

    console.log(playerLocation);
    //console.log("old: " + previousLocation);
    //console.log("pos: " + oldPosition);
    // Fill in player's previous location
    //context.fillStyle = wallColor;
    //context.fillRect(oldPosition[1], oldPosition[0], size/mapSize, size/mapSize);

    // Draw new location
    context.fillStyle = playerColor;
    context.fillRect(newPosition[1], newPosition[0], size/mapSize, size/mapSize);
}
